﻿using System;

namespace Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Variables
            bool legalplacement = true;
            Console.Write("What is the size of the outerrows");
            int outerrow = Convert.ToInt16(Console.ReadLine());
            Console.Write("What is the size of the outercollumns");
            int outercollumn = Convert.ToInt16(Console.ReadLine());
            Console.Write("What is the size of the innerrows");
            int innerrowsize = Convert.ToInt16(Console.ReadLine());
            Console.Write("What is the size of the innercollumns");
            int innercollumnsize = Convert.ToInt16(Console.ReadLine());
            if (innerrowsize > outerrow -4 || innercollumnsize > outercollumn - 4)
            {
                Console.WriteLine("The inner row is to big compared too the outer row");
                legalplacement = false;
            }
            int innerrowstart = 2;
            int innercollumnstart = 2;
            bool checkside = true;
            string[,] rectangle = new string[outerrow, outercollumn];
            #endregion

            #region Create frame
            for (int i = 0; i < outerrow; i++)
            {
                for (int j = 0; j < outercollumn; j++)
                {
                    if (i == 0)
                    {
                        checkside = true;
                    }
                    if (j == 0)
                    {
                        checkside = true;
                    }
                    if (i == outerrow - 1)
                    {
                        checkside = true;
                    }
                    if (j == outercollumn - 1)
                    {
                        checkside = true;
                    }
                    if (checkside == true)
                    {
                        rectangle[i, j] = "#";
                    }
                    else
                    {
                        rectangle[i, j] = " ";
                    }

                    if (checkside == true)
                    {
                        rectangle[i, j] = "#";
                    }
                    else
                    {
                        rectangle[i, j] = " ";
                    }
                    checkside = false;

                    #endregion
                    //Create inner square
                    //Create top row
                    for (int m = 0; m < innercollumnsize; m++)
                    {
                        rectangle[innerrowstart, innercollumnstart + m] = "#";
                    }
                    
                    //Create left side
                    for (int n = 0; n < innerrowsize; n++)
                    {
                        rectangle[innerrowstart + n, innercollumnstart] = "#";
                    }
                    
                    //Create bottumn
                    
                    for (int o = 0; o < innercollumnsize; o++)
                    {
                        rectangle[innerrowstart + innerrowsize-1, innercollumnstart + o] = "#";
                    }
                    
                    //Create right side
                    for (int p = 0; p < innerrowsize; p++)
                    {
                        rectangle[innerrowstart + p, innercollumnstart + innercollumnsize-1] = "#";
                    }
                }
            }
            //Check if the position is too close to the edges
            if (innerrowstart < 2 || innercollumnstart < 2)
            {
                legalplacement = false;
            }
            if (innerrowstart + innerrowsize + 2 > outerrow || innercollumnsize + innercollumnstart + 2 > outercollumn)
            {
                legalplacement = false;
            }
            if (legalplacement == false)
            {
                Console.WriteLine("The innerrectangle is too close to the edge");
            }
            //If legal placement create inner rectangle
            if (legalplacement == true)
            {
                //Printmatrix
                Console.WriteLine("Printing Matrix: ");
                for (int k = 0; k < outerrow; k++)
                {
                    for (int l = 0; l < outercollumn; l++)
                    {
                        Console.Write(rectangle[k, l]);
                    }
                    Console.WriteLine();
                }
                Console.ReadLine();
            }
        }
    }
}