﻿using System;

namespace Task8c
{
    class Program
    {
        static void Main(string[] args)
        {
            int outerrow = Findouterrow();
            int outercollumn = Findoutercollumn();
            int innerrowsize = Findinnerrow();
            int innercollumnsize = Findinnercollumn();
            bool legalplacement = Checkiftheyfit(outerrow, outercollumn, innerrowsize, innercollumnsize);
            string[,] rectangle = Makerectangle(outerrow, outercollumn, innerrowsize, innercollumnsize);
            Printrectangle(rectangle, outerrow, outercollumn);
        }
        public static int Findouterrow()
        {
            Console.Write("What is the size of the outerrows");
            int outerrow = Convert.ToInt16(Console.ReadLine());
            return outerrow;
        }
        public static int Findoutercollumn()
        {
            Console.Write("What is the size of the outercollumns");
            int outercollumn = Convert.ToInt16(Console.ReadLine());
            return outercollumn;
        }
        public static int Findinnerrow()
        {
            Console.Write("What is the size of the innerrows");
            int innerrowsize = Convert.ToInt16(Console.ReadLine());
            return innerrowsize;
        }
        public static int Findinnercollumn()
        {
            Console.Write("What is the size of the innercollumns");
            int innercollumnsize = Convert.ToInt16(Console.ReadLine());
            return innercollumnsize;
        }
        public static bool Checkiftheyfit(int outerrow, int outercollumn, int innerrowsize, int innercollumnsize)
        {
            bool legalplacement = true;
            if (innerrowsize > outerrow - 4 || innercollumnsize > outercollumn - 4)
            {
                Console.WriteLine("The inner row is too big compared to the outer row");
                legalplacement = false;
            }
            return legalplacement;
        }
        public static string[,] Makerectangle(int outerrow, int outercollumn, int innerrowsize, int innercollumnsize)
        {
            string[,] rectangle = new string[outerrow, outercollumn];
            bool checkside = true;
            int innerrowstart = 2;
            int innercollumnstart = 2;
            for (int i = 0; i < outerrow; i++)
            {
                for (int j = 0; j < outercollumn; j++)
                {
                    if (i == 0)
                    {
                        checkside = true;
                    }
                    if (j == 0)
                    {
                        checkside = true;
                    }
                    if (i == outerrow - 1)
                    {
                        checkside = true;
                    }
                    if (j == outercollumn - 1)
                    {
                        checkside = true;
                    }
                    if (checkside == true)
                    {
                        rectangle[i, j] = "#";
                    }
                    else
                    {
                        rectangle[i, j] = " ";
                    }

                    if (checkside == true)
                    {
                        rectangle[i, j] = "#";
                    }
                    else
                    {
                        rectangle[i, j] = " ";
                    }
                    checkside = false;
                    //Create inner square
                    //Create top row
                    for (int m = 0; m < innercollumnsize; m++)
                    {
                        rectangle[innerrowstart, innercollumnstart + m] = "#";
                    }
                    //Create left side
                    for (int n = 0; n < innerrowsize; n++)
                    {
                        rectangle[innerrowstart + n, innercollumnstart] = "#";
                    }
                    //Create bottumn

                    for (int o = 0; o < innercollumnsize; o++)
                    {
                        rectangle[innerrowstart + innerrowsize - 1, innercollumnstart + o] = "#";
                    }

                    //Create right side
                    for (int p = 0; p < innerrowsize; p++)
                    {
                        rectangle[innerrowstart + p, innercollumnstart + innercollumnsize - 1] = "#";
                    }
                }
            }
            return rectangle;
        }

        public static void Printrectangle(string[,] rectangle, int outerrow, int outercollumn)
        {
            Console.WriteLine("Printing Matrix: ");
            for (int k = 0; k < outerrow; k++)
            {
                for (int l = 0; l < outercollumn; l++)
                {
                    Console.Write(rectangle[k, l]);
                }
                Console.WriteLine();
            }
            Console.ReadLine();
        }
    }
}
